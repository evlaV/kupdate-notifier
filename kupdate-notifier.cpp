/*
 * SPDX-License-Identifier: LGPL-2.1+
 *
 * Copyright © 2020-2021 Collabora Ltd.
 *
 * This file is part of kupdate-notifier.
 *
 * kupdate-notifier is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * This file is based on knotifications (tests/kstatusnotifieritemtest.cpp).
 *
 * Copyright 2009 by Marco Martin <notmart@gmail.com>
 */

#include "kupdate-notifier.h"

#include <QApplication>
#include <QCommandLineParser>
#include <QDebug>
#include <QLabel>
#include <QMenu>
#include <QSessionManager>

#include <KConfig>
#include <KConfigGroup>

UpdateStatusNotifierItem::UpdateStatusNotifierItem(QObject *parent, const QString &actionScript, int timerMilliSecond,
                                                   bool onlineUpdates, bool offlineUpdates, const QString &title,
                                                   const QString &subTitle, const QString &icon,
                                                   const QString &overlayIcon, const QString &attentionIcon,
                                                   const QString &updateAvailableIcon)
    : QObject(parent)
    , m_updateAvailableIcon(updateAvailableIcon)
    , m_checkForUpdates(QIcon::fromTheme(QStringLiteral("system-software-update")),
                        QStringLiteral("Check for updates"),
                        m_tray.contextMenu())
    , m_onlineUpdates(QIcon::fromTheme(QStringLiteral("update-none")),
                      QStringLiteral("Update now"),
                      m_tray.contextMenu())
    , m_offlineUpdates(QIcon::fromTheme(QStringLiteral("system-reboot")),
                       QStringLiteral("Reboot and update"),
                       m_tray.contextMenu())
    , m_actionScript(actionScript)
    , m_timerMilliSecond(timerMilliSecond)
{
    m_tray.setTitle(title);
    m_tray.setIconByName(icon);
    m_tray.setAttentionIconByName(attentionIcon);
    m_tray.setOverlayIconByName(overlayIcon);
    m_tray.setStatus(KStatusNotifierItem::Passive);
    m_tray.setToolTip(icon, title, subTitle);
    m_tray.setStandardActionsEnabled(false);

    m_checkForUpdates.setIcon(QIcon::fromTheme(QStringLiteral("system-software-update")));
    m_onlineUpdates.setIcon(QIcon::fromTheme(QStringLiteral("update-none")));
    m_offlineUpdates.setIcon(QIcon::fromTheme(QStringLiteral("system-reboot")));

    connect(&m_process, QOverload<QProcess::ProcessError>::of(&QProcess::errorOccurred), this, &UpdateStatusNotifierItem::errorOccurred);
    connect(&m_process, QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished), this, &UpdateStatusNotifierItem::finished);
    connect(&m_process, SIGNAL(readyReadStandardOutput()), this, SLOT(readyReadStandardOutput()));
    connect(&m_process, SIGNAL(readyReadStandardError()), this, SLOT(readyReadStandardError()));
    connect(&m_process, SIGNAL(started()), this, SLOT(started()));
    connect(&m_process, SIGNAL(stateChanged(QProcess::ProcessState)), this, SLOT(stateChanged(QProcess::ProcessState)));

    QMenu *menu = m_tray.contextMenu();
    menu->addAction(&m_checkForUpdates);
    if (onlineUpdates)
        menu->addAction(&m_onlineUpdates);
    if (offlineUpdates)
        menu->addAction(&m_offlineUpdates);

    connect(&m_checkForUpdates, &QAction::triggered, this, &UpdateStatusNotifierItem::checkForUpdates);
    connect(&m_onlineUpdates, &QAction::triggered, this, &UpdateStatusNotifierItem::onlineUpdates);
    connect(&m_offlineUpdates, &QAction::triggered, this, &UpdateStatusNotifierItem::offlineUpdates);

    m_upToDate = true;
    m_onlineUpdates.setDisabled(m_upToDate);
    m_offlineUpdates.setDisabled(m_upToDate);

    m_scheduledCheckForUpdates = false;
    m_timer.start(0);
    QObject::connect(&m_timer, SIGNAL(timeout()), this, SLOT(timeout()));
}

void UpdateStatusNotifierItem::timeout()
{
    if (m_process.state() != QProcess::NotRunning)
        return;

    m_scheduledCheckForUpdates = true;
    checkForUpdates();
}

void UpdateStatusNotifierItem::checkForUpdates()
{
    if (m_process.state() != QProcess::NotRunning) {
        if (m_checkingForUpdates) {
            qInfo() << "Cancel checking for updates...";
            m_process.terminate();
        }
        return;
    }

    m_checkingForUpdates = true;
    m_isOnlineUpdates = false;
    m_process.start(m_actionScript, QStringList() << "has-updates");
}

void UpdateStatusNotifierItem::onlineUpdates()
{
    if (m_process.state() != QProcess::NotRunning)
        return;

    m_checkingForUpdates = false;
    m_isOnlineUpdates = true;
    m_process.start(m_actionScript, QStringList() << "online-updates");
}

void UpdateStatusNotifierItem::offlineUpdates()
{
    if (m_process.state() != QProcess::NotRunning)
        return;

    m_checkingForUpdates = false;
    m_isOnlineUpdates = false;
    m_process.start(m_actionScript, QStringList() << "offline-updates");
}

void UpdateStatusNotifierItem::errorOccurred(QProcess::ProcessError error)
{
    qWarning() << "An error occured with process!" << error << m_process.errorString();
}

void UpdateStatusNotifierItem::finished(int exitCode, QProcess::ExitStatus exitStatus)
{
    KStatusNotifierItem::ItemStatus status = KStatusNotifierItem::Passive;
    QString title = QStringLiteral("System up-to-date");
    QString message = QStringLiteral("The system is up-to-date!");
    QString icon = m_tray.toolTipIconName();
    int timeout = 3000;

    if (exitStatus != QProcess::NormalExit || exitCode == 127) {
        qWarning() << "Process has finished not normally!" << exitCode << exitStatus;
        m_onlineUpdates.setDisabled(m_upToDate);
        m_offlineUpdates.setDisabled(m_upToDate);
        return;
    }

    if (not m_checkingForUpdates) {
        if (m_isOnlineUpdates) {
            m_upToDate = exitCode != 0;
            m_onlineUpdates.setIcon(QIcon::fromTheme(QStringLiteral("update-none")));
            m_onlineUpdates.setDisabled(m_upToDate);
            if (m_upToDate)
                m_onlineUpdates.setText(QString("Update completed!"));
            else
                m_onlineUpdates.setText(QString("Update failed!"));
            m_offlineUpdates.setDisabled(m_upToDate);
        }

        return;
    }

    if (exitCode == 0) {
        status = m_upToDate ? KStatusNotifierItem::NeedsAttention : KStatusNotifierItem::Active;
        title = QStringLiteral("Updates available");
        message = QStringLiteral("Updates are available!");
        icon = m_updateAvailableIcon;
        timeout = 5000;
    }

    if (m_output.length() > 0) {
        message = m_output;
        m_output.clear();
    }

    m_tray.setIconByName(icon);
    m_tray.setStatus(status);
    if (exitCode == 0 or not m_scheduledCheckForUpdates)
        m_tray.showMessage(title, message, icon, timeout);
    m_scheduledCheckForUpdates = false;
    m_upToDate = exitCode != 0;
    m_onlineUpdates.setIcon(QIcon::fromTheme(QStringLiteral("update-low")));
    m_onlineUpdates.setDisabled(m_upToDate);
    m_onlineUpdates.setText(QStringLiteral("Update now"));
    m_offlineUpdates.setDisabled(m_upToDate);
}

void UpdateStatusNotifierItem::readyReadStandardOutput()
{
    QByteArray data = m_process.readAllStandardOutput();
    QString output = QString(data).trimmed().toLatin1().data();
    qInfo() << qPrintable(output);
    if (not m_checkingForUpdates and not m_isOnlineUpdates)
        return;

    if (m_checkingForUpdates) {
        m_output = output;
        return;
    }

    m_onlineUpdates.setText(QString("Update in progress... ") + output);
}

void UpdateStatusNotifierItem::readyReadStandardError()
{
    QByteArray data = m_process.readAllStandardError();
    QString output = QString(data).trimmed().toLatin1().data();
    qInfo() << qPrintable(output);
}

void UpdateStatusNotifierItem::started()
{
    if (not m_checkingForUpdates) {
        m_onlineUpdates.setDisabled(true);
        m_offlineUpdates.setDisabled(true);
        return;
    }
}

void UpdateStatusNotifierItem::stateChanged(QProcess::ProcessState newState)
{
    if (not m_checkingForUpdates) {
        if (m_isOnlineUpdates)
            if (newState == QProcess::NotRunning)
                m_onlineUpdates.setText("Online updates");
            else
                m_onlineUpdates.setText(QString("Update started..."));
        m_onlineUpdates.setDisabled(false);
        m_offlineUpdates.setDisabled(false);
        return;
    }

    if (newState != QProcess::NotRunning) {
        m_checkForUpdates.setText("Cancel");
        m_checkForUpdates.setIcon(QIcon::fromTheme(QStringLiteral("process-stop")));
        m_timer.stop();
        return;
    }

    m_checkForUpdates.setText("Check for updates");
    m_checkForUpdates.setIcon(QIcon::fromTheme(QStringLiteral("system-software-update")));
    m_timer.start(m_timerMilliSecond);
}

int main(int argc, char **argv)
{
    QApplication app(argc, argv);

    QCommandLineParser parser;
    parser.setApplicationDescription(QCoreApplication::translate("main", "Update Notifier for the new systemtray specification."));
    parser.addHelpOption();
    parser.addPositionalArgument("[action-script]", QCoreApplication::translate("main", "The action script to run."));
    parser.addPositionalArgument("[timer-second]", QCoreApplication::translate("main", "The periodical timer to check for updates."));
    parser.addOption(QCommandLineOption(QStringList() << QStringLiteral("online-updates"),
                                        QCoreApplication::translate("main", "Enable online-updates.")));
    parser.addOption(QCommandLineOption(QStringList() << QStringLiteral("no-online-updates"),
                                        QCoreApplication::translate("main", "Disable online-updates.")));
    parser.addOption(QCommandLineOption(QStringList() << QStringLiteral("offline-updates"),
                                        QCoreApplication::translate("main", "Enable offline-updates.")));
    parser.addOption(QCommandLineOption(QStringList() << QStringLiteral("no-offline-updates"),
                                        QCoreApplication::translate("main", "Disable offline-updates.")));
    parser.addOption(QCommandLineOption(QStringList() << QStringLiteral("tooltip-title"),
                                        QCoreApplication::translate("main", "Sets the title for the tooltip."),
                                        QStringLiteral("title"),
                                        QStringLiteral("KUpdate Notifier")));
    parser.addOption(QCommandLineOption(QStringList() << QStringLiteral("tooltip-subtitle"),
                                        QCoreApplication::translate("main", "Sets the subtitle for the tooltip."),
                                        QStringLiteral("subtitle"),
                                        QStringLiteral("This is a test of the new systemtray specification")));
    parser.addOption(QCommandLineOption(QStringList() << QStringLiteral("icon"),
                                        QCoreApplication::translate("main", "Sets the main icon for the system tray."),
                                        QStringLiteral("icon"),
                                        QStringLiteral("update-none")));
    parser.addOption(QCommandLineOption(QStringList() << QStringLiteral("overlay-icon"),
                                        QCoreApplication::translate("main", "Sets the icon to used as overlay for the main one."),
                                        QStringLiteral("icon"),
                                        QStringLiteral("emblem-important")));
    parser.addOption(QCommandLineOption(QStringList() << QStringLiteral("attention-icon"),
                                        QCoreApplication::translate("main", "Sets the icon to request attention."),
                                        QStringLiteral("icon"),
                                        QStringLiteral("update-high")));
    parser.addOption(QCommandLineOption(QStringList() << QStringLiteral("update-available-icon"),
                                        QCoreApplication::translate("main", "Sets the main icon for the system tray if updates are available"),
                                        QStringLiteral("icon"),
                                        QStringLiteral("update-low")));
    parser.process(app);

    KConfig config(QStringLiteral("kupdatenotifierrc"), KConfig::SimpleConfig);
    KConfigGroup group = config.group("General");

    QString actionScript = group.readEntry<QString>(QStringLiteral("ActionScript"),
                                                    QStringLiteral("kupdate-notifier.action"));
    int timerSecond = group.readEntry<int>(QStringLiteral("TimerSecond"),
                                           3600);
    bool onlineUpdates = group.readEntry<bool>(QStringLiteral("OnlineUpdates"),
                                               false);
    bool offlineUpdates = group.readEntry<bool>(QStringLiteral("OfflineUpdates"),
                                                false);
    QString toolTipTitle = group.readEntry<QString>(QStringLiteral("ToolTipTitle"),
                                                    QStringLiteral("KUpdate Notifier"));
    QString toolTipSubTitle = group.readEntry<QString>(QStringLiteral("ToolTipSubTitle"),
                                                       QStringLiteral("This is a test of the new systemtray specification"));
    QString icon = group.readEntry<QString>(QStringLiteral("Icon"),
                                            QStringLiteral("update-none"));
    QString overlayIcon = group.readEntry<QString>(QStringLiteral("OverlayIcon"),
                                                   QStringLiteral("emblem-important"));
    QString attentionIcon = group.readEntry<QString>(QStringLiteral("AttentionIcon"),
                                                     QStringLiteral("update-high"));
    QString updateAvailableIcon = group.readEntry<QString>(QStringLiteral("UpdateAvailableIcon"),
                                                           QStringLiteral("update-low"));

    if (parser.isSet(QStringLiteral("online-updates")))
        onlineUpdates = true;
    else if (parser.isSet(QStringLiteral("no-online-updates")))
        onlineUpdates = false;

    if (parser.isSet(QStringLiteral("offline-updates")))
        offlineUpdates = true;
    else if (parser.isSet(QStringLiteral("no-offline-updates")))
        offlineUpdates = false;

    if (parser.isSet(QStringLiteral("tooltip-title")))
        toolTipTitle = parser.value(QStringLiteral("tooltip-title"));

    if (parser.isSet(QStringLiteral("tooltip-subtitle")))
        toolTipSubTitle = parser.value(QStringLiteral("tooltip-subtitle"));

    if (parser.isSet(QStringLiteral("icon")))
        icon = parser.value(QStringLiteral("icon"));

    if (parser.isSet(QStringLiteral("overlay-icon")))
        overlayIcon = parser.value(QStringLiteral("overlay-icon"));

    if (parser.isSet(QStringLiteral("attention-icon")))
        attentionIcon = parser.value(QStringLiteral("attention-icon"));

    if (parser.isSet(QStringLiteral("update-available-icon")))
        updateAvailableIcon = parser.value(QStringLiteral("update-available-icon"));

    if (parser.positionalArguments().count() > 0)
        actionScript = parser.positionalArguments()[0];

    if (parser.positionalArguments().count() > 1)
        timerSecond = parser.positionalArguments()[1].toInt();

    UpdateStatusNotifierItem *item = new UpdateStatusNotifierItem(nullptr,
                                                                  actionScript,
                                                                  timerSecond * 1000,
                                                                  onlineUpdates,
                                                                  offlineUpdates,
                                                                  toolTipTitle,
                                                                  toolTipSubTitle,
                                                                  icon,
                                                                  overlayIcon,
                                                                  attentionIcon,
                                                                  updateAvailableIcon);

    auto disableSessionManagement = [](QSessionManager &sm) {
        sm.setRestartHint(QSessionManager::RestartNever);
    };

    QObject::connect(&app, &QGuiApplication::commitDataRequest, disableSessionManagement);
    QObject::connect(&app, &QGuiApplication::saveStateRequest, disableSessionManagement);

    return app.exec();
}

