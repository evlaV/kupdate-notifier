/*
 * SPDX-License-Identifier: LGPL-2.1+
 *
 * Copyright © 2020-2021 Collabora Ltd.
 *
 * This file is part of kupdate-notifier.
 *
 * kupdate-notifier is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 *
 * This file is based on knotifications (tests/kstatusnotifieritemtest.h).
 *
 * Copyright 2009 by Marco Martin <notmart@gmail.com>
 */

#ifndef UPDATESTATUSNOTIFIERITEM_H
#define UPDATESTATUSNOTIFIERITEM_H

#include <kstatusnotifieritem.h>

#include <QObject>
#include <QTimer>
#include <QAction>
#include <QProcess>

class UpdateStatusNotifierItem : public QObject
{
    Q_OBJECT

public:
    UpdateStatusNotifierItem(QObject *parent, const QString &actionScript, int timerMilliSecond, bool onlineUpdates,
                             bool offlineUpdates, const QString &title, const QString &subTitle, const QString &icon,
                             const QString &overlayIcon, const QString &attentionIcon,
                             const QString &updateAvailableIcon);

public Q_SLOTS:
    void timeout();
    void checkForUpdates();
    void onlineUpdates();
    void offlineUpdates();
    void errorOccurred(QProcess::ProcessError error);
    void finished(int exitCode, QProcess::ExitStatus exitStatus);
    void readyReadStandardOutput();
    void readyReadStandardError();
    void started();
    void stateChanged(QProcess::ProcessState newState);
private:
    QString m_updateAvailableIcon;
    KStatusNotifierItem m_tray;
    QString m_output;
    bool m_scheduledCheckForUpdates;
    QTimer m_timer;
    bool m_checkingForUpdates;
    QAction m_checkForUpdates;
    bool m_upToDate;
    bool m_isOnlineUpdates;
    QAction m_onlineUpdates;
    QAction m_offlineUpdates;
    QProcess m_process;
    QString m_actionScript;
    int m_timerMilliSecond;
};

#endif
