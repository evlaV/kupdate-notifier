#
#  SPDX-License-Identifier: LGPL-2.1+
#
#  Copyright © 2020-2021 Collabora Ltd.
#
#  This file is part of kupdate-notifier.
#
#  kupdate-notifier is free software; you can redistribute it and/or modify it
#  under the terms of the GNU Lesser General Public License as published by the
#  Free Software Foundation; either version 2.1 of the License, or (at your
#  option) any later version.

.PHONY: all
all: kupdate-notifier

kupdate-notifier: moc_kupdate-notifier.cpp
kupdate-notifier: override CXXFLAGS += -fPIC
kupdate-notifier: override CXXFLAGS += $$(pkg-config --cflags Qt5Core Qt5Widgets Qt5Gui)
kupdate-notifier: override LDLIBS += $$(pkg-config --libs Qt5Core Qt5Widgets Qt5Gui)
kupdate-notifier: override CXXFLAGS += -I /usr/include/KF5/KNotifications
kupdate-notifier: override CXXFLAGS += -I /usr/include/KF5/KConfigCore
kupdate-notifier: override LDLIBS += -lKF5Notifications
kupdate-notifier: override LDLIBS += -lKF5ConfigCore

.PHONY: run
run: PATH := $(CURDIR):$(PATH)
run: kupdate-notifier
	kupdate-notifier kupdate-notifier.action 3600

.PHONY: install
install: PREFIX ?= /usr/local
install:
	install -Dm755 kupdate-notifier $(DESTDIR)$(PREFIX)/bin/kupdate-notifier
	install -Dm755 kupdate-notifier.action $(DESTDIR)$(PREFIX)/share/kupdate-notifier/examples/kupdate-notifier.action
	install -Dm644 kupdate-notifier.service $(DESTDIR)$(PREFIX)/share/kupdate-notifier/examples/kupdate-notifier.service
	completionsdir=$${BASHCOMPLETIONSDIR:-$$(pkg-config --define-variable=prefix=$(PREFIX) \
	                             --variable=completionsdir \
	                             bash-completion)}; \
	if [ -n "$$completionsdir" ]; then \
		install -D -m 644 bash-completion $(DESTDIR)$$completionsdir/kupdate-notifier; \
	fi

.PHONY: clean
clean:
	rm -f kupdate-notifier moc_kupdate-notifier.cpp

moc_%.cpp: %.h
	moc $(DEFINES) $(INCPATH) $< -o $@
